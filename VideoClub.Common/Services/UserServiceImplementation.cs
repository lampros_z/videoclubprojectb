﻿using System;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using VideoClub.Core.Entities;
using VideoClub.Core.Interfaces;
using VideoClub.Core.Utilities.Areas.Controllers;
using VideoClub.Infrastructure.Data;
using VideoClub.Infrastructure.Services.Interfaces;

namespace VideoClub.Common.Services
{
    public class UserServiceImplementation : IUserService
    {
        private readonly ClientUserDbContext _dbContext;
        private readonly ILoggingService _loggingService;

        public UserServiceImplementation(ClientUserDbContext dbContext, ILoggingService loggingService)
        {
            _dbContext = dbContext;
            _loggingService = loggingService;
        }

        public ClientUser[] FetchClientUsers(PaginationInfo paginationInfo)
        {
            try
            {
                return TryFetchingClientUsers(paginationInfo);
            }
            catch (ArgumentNullException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return new ClientUser[0];
            }
        }

        private ClientUser[] TryFetchingClientUsers(PaginationInfo paginationInfo) =>
            _dbContext.Users.Where(u => u.UserName != "Admin")
            .OrderBy(u => u.UserName)
            .Skip(paginationInfo.NumberOfRecordsPerPage * (paginationInfo.CurrentPage - 1))
            .Take(paginationInfo.NumberOfRecordsPerPage)
            .ToArray();



        public MovieRentalRecord[] FetchClientRentalRecordHistory(string userId, PaginationInfo paginationInfo)
        {
            try
            {
                return TryFetchingClientRentalRecordHistory(userId, paginationInfo);
            }
            catch (ArgumentNullException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return new MovieRentalRecord[0];
            }
        }


        private MovieRentalRecord[] TryFetchingClientRentalRecordHistory(string userId, PaginationInfo paginationInfo)
        {
            var user = FindClientUserById(userId);
            return user.movieRentalRecords
                .OrderByDescending(r => r.RentDate)
                .Skip(paginationInfo.NumberOfRecordsPerPage * (paginationInfo.CurrentPage - 1))
                .Take(paginationInfo.NumberOfRecordsPerPage)
                .ToArray();
        }

        public MovieRentalRecord[] FetchClientActiveRentalRecords(string userId, PaginationInfo paginationInfo)
        {
           try
           {
                return TryFetchingClientActiveRentalRecords(userId, paginationInfo);
           }
           catch (ArgumentNullException e) 
           {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return new MovieRentalRecord[0];
           }
        }

        private MovieRentalRecord[] TryFetchingClientActiveRentalRecords(string userId, PaginationInfo paginationInfo)
        {
            var user = FindClientUserById(userId);
            return user.movieRentalRecords
                .Where( r => r.Returned == false )
                .OrderByDescending(r => r.RentDate)
                .Skip(paginationInfo.NumberOfRecordsPerPage*(paginationInfo.CurrentPage - 1))
                .Take(paginationInfo.NumberOfRecordsPerPage)
                .ToArray();
        }
        
        public int CountClientActiveRentalRecords(string userId)
        {
            try
            {
                return TryCountingClientActiveRentalRecords(userId);
            }
            catch (ArgumentNullException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return 0;
            }
        }

        public int CountUserRentalRecords(string userId)
        {
            try
            {
                return TryCountingUserRentalRecords(userId);
            }
            catch (ArgumentNullException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return 0;
            }
        }

        private int TryCountingUserRentalRecords(string userId) => _dbContext.Users.Where(u => u.Id == userId).SelectMany(u => u.movieRentalRecords).Count();

            private int TryCountingClientActiveRentalRecords(string userId)
        {
            var user = FindClientUserById(userId);
            return user.movieRentalRecords
                .Count(r => r.Returned == false);
        }

        public ClientUser FindClientUserById(string id) => _dbContext.Users.Find(id) ?? new ClientUser();

        public MovieRentalRecord[] FetchAllActiveRentalRecords(PaginationInfo paginationInfo)
        {
            try
            {
                return TryFetchingAllActiveRentalRecords(paginationInfo);
            }
            catch (ArgumentNullException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return new MovieRentalRecord[0];
            }
        }

        private MovieRentalRecord[] TryFetchingAllActiveRentalRecords(PaginationInfo paginationInfo) =>  
            _dbContext.Users
            .SelectMany(u => u.movieRentalRecords)
            .Where(r => r.Returned == false)
            .OrderBy(r => r.RentDate)
            .Skip(paginationInfo.NumberOfRecordsPerPage*(paginationInfo.CurrentPage-1))
            .Take(paginationInfo.NumberOfRecordsPerPage)
            .ToArray();

        public int CountAllActiveRentalRecords()
        {
            try
            {
                return TryCountingAllActiveRentalRecords();
            }
            catch (ArgumentNullException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return 0;
            }
            catch (OverflowException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return 0;
            }
        }

        private int TryCountingAllActiveRentalRecords() => _dbContext.Users
            .SelectMany(u => u.movieRentalRecords)
            .Count(r => r.Returned == false);

        public MovieRentalRecord[] FetchUserActiveRentalRecords(string userId, PaginationInfo paginationInfo)
        {
            try
            {
                return TryFetchingUserActiveRentalRecords(userId, paginationInfo);
            }
            catch (ArgumentNullException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return new MovieRentalRecord[0];
            }
        }

        private MovieRentalRecord[]  TryFetchingUserActiveRentalRecords(string userId, PaginationInfo paginationInfo)
        {
            var user = FindClientUserById(userId);
            return user.movieRentalRecords
                .Where(r => r.Returned == false)
                .OrderByDescending(r => r.RentDate)
                .Skip(paginationInfo.NumberOfRecordsPerPage * (paginationInfo.CurrentPage - 1))
                .Take(paginationInfo.NumberOfRecordsPerPage)
                .ToArray();
        }

        public MovieRentalRecord RegisterUsersMovieRentalRecord(RentalRecordInfo info)
        {
            try
            {
                return TryRegisteringUsersMovieRentalRecord( info );
            }
            catch(Exception e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return new MovieRentalRecord();
            }
        }

        private MovieRentalRecord TryRegisteringUsersMovieRentalRecord(RentalRecordInfo info)
        {
            var user = FindClientUserById(info.UserId);
            var RentalRecord = new MovieRentalRecord
            {
                RentDate = DateTime.Today,
                ReturnDate = DateTime.Today.AddDays(7),
                Comment = info.CommentText,
                Returned = false,
                ClientUserId = info.UserId,
            };
            user.movieRentalRecords.Add(RentalRecord);
            Commit();
            return RentalRecord;
        }

        private void Commit()
        {
            _dbContext.SaveChanges();
        }

        public ClientUser FindClientUserByUsername(string userName)
        {
            try
            {
                return TryFindUserByUsername(userName);
            }
            catch (ArgumentNullException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return new ClientUser { UserName = "unknown" };
            }
            catch (InvalidOperationException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return new ClientUser { UserName = "unknown" };
            }
        }

        private ClientUser TryFindUserByUsername(string userName) => _dbContext.Users.First(u => u.UserName == userName);

        public void CreateUser(ClientUser clientUser)
        {
            try
            {
                TryPersistingUser(clientUser);
            }
            catch (Exception e)
            {
                var ReflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", ReflectedFunctionName, e.Message);
            }
        }

        private void TryPersistingUser(ClientUser clientUser)
        {
            _dbContext.Users.Add(clientUser);
            Commit();
        }

        public void UpdateUserRoles(ClientUser clientUser)
        {
            try
            {
                TryUpdatingUser(clientUser);
            }
            catch (Exception e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
            }
        }

        private void TryUpdatingUser(ClientUser clientUser)
        {
            _dbContext.Users.Attach(clientUser);
            _dbContext.Entry(clientUser).Property("Roles").IsModified = true;
            Commit();
        }

        public int GetTotalNumberOfClientUsers()
        {
            try
            {
                return TryGettingTotalNumberOfClientUsers();
            }
            catch (ArgumentNullException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return 0;
            }
        }

        private int TryGettingTotalNumberOfClientUsers() => _dbContext.Users.Count(u => u.UserName != "Admin");

        public void UpdateUserRentalRecords(ClientUser clientUser)
        {
            try
            {
                TryUpdatingClientUserRentalRecords(clientUser);
            }
            catch (Exception e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
            }
        }

        private void TryUpdatingClientUserRentalRecords(ClientUser clientUser)
        {
            _dbContext.Users.Attach(clientUser);
            _dbContext.Entry(clientUser).Property("movieRentalRecords").IsModified = true;
            Commit();
        }
    }

}
