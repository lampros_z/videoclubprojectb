﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using VideoClub.Core.Entities;
using VideoClub.Core.Extensions;
using VideoClub.Core.Interfaces;
using VideoClub.Core.Utilities.Areas.Controllers;
using VideoClub.Infrastructure.Data;
using VideoClub.Infrastructure.Services.Interfaces;

namespace VideoClub.Common.Services
{
    public class MovieServiceImplementation : IMovieService
    {
        private readonly  MovieDbContext _dbContext;
        private readonly ILoggingService _loggingService;
        public MovieServiceImplementation(MovieDbContext dbContext, ILoggingService loggingService)
        {
            _dbContext = dbContext;
            _loggingService = loggingService;
        }


        public Movie[] FetchMovies(Core.Utilities.Areas.Controllers.MovieFilters movieFilters, PaginationInfo paginationInfo)
        {
            try
            {
                return TryFetchingMovies(movieFilters, paginationInfo);
            }
            catch (ArgumentNullException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return new Movie[0];
            }
        }

        private Movie[] TryFetchingMovies(Core.Utilities.Areas.Controllers.MovieFilters movieFilters, PaginationInfo paginationInfo) =>
                    _dbContext.Movies
                    .HandleTitleFiltering(movieFilters)
                    .HandleAvailabilityFilter(movieFilters)
                    .HandleGenreFilter(movieFilters)
                    .OrderBy(m => m.Id)
                    .Skip(paginationInfo.NumberOfRecordsPerPage * (paginationInfo.CurrentPage - 1))
                    .Take(paginationInfo.NumberOfRecordsPerPage)
                    .ToArray();

        
        
        public void AddMovie(Movie movie)
        {
            try
            {
                PersistMovie(movie);
            }
            catch(Exception e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
            }
        }

        private void PersistMovie(Movie movie)
        {
            _dbContext.Movies.Add(movie);
            Commit();
        }


        public Movie[] GetMoviesWithAvailableMovieCopiesByTitle(string title)
        {
            try
            {
                return TryGettingMoviesWithAvailableMovieCopiesByTitle(title);
            }
            catch(ArgumentNullException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return new Movie[0];
            }
        }

        private Movie[] TryGettingMoviesWithAvailableMovieCopiesByTitle(string title) =>
            _dbContext.Movies
            .HandleTitleFiltering(title)
            .Where(m => m.MovieCopies.Any())
            .Take(50)
            .ToArray();

        public MovieCopy[] FetchMovieCopies(MovieRentalRecordBindModel[] model)
        {
            try
            {
                return TryFetchingMovieCopies(model);
            }
            catch (ArgumentNullException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return new MovieCopy[0];
            }
        }

        private MovieCopy[] TryFetchingMovieCopies(MovieRentalRecordBindModel[] model)
        {
            var MovieCopies = new List<MovieCopy>();
            foreach(var movieInfo in model)
            {
                var copies = _dbContext.MovieCopies.Where(mc => mc.MovieId == movieInfo.Id).Where(mc => mc.IsAvailable).Take(movieInfo.Quantity).ToList();
                MovieCopies.AddRange(copies);
            }
            return MovieCopies.ToArray();
        }

        public void UpdateMovieCopy(MovieCopy movieCopy)
        {
            try
            {
                TryUpdatingMovieCopy(movieCopy);
            }
            catch (Exception e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
            }
        }

        private void TryUpdatingMovieCopy(MovieCopy movieCopy)
        {
            _dbContext.MovieCopies.AddOrUpdate(movieCopy);
            Commit();
        }

        public void BatchUpdateMovieCopies(MovieCopy[] movieCopies)
        {
            try
            {
                TryUpdatingMovieCopies(movieCopies);
            }
            catch (Exception e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
            }
        }

        private void TryUpdatingMovieCopies(MovieCopy[] movieCopies)
        {
            foreach(var MovieCopy in movieCopies)
            {
                _dbContext.MovieCopies.AddOrUpdate(MovieCopy);
            }
            Commit();
        }

        private void Commit()
        {
            _dbContext.SaveChanges();
        }

        public int CountMovieRecords(Core.Utilities.Areas.Controllers.MovieFilters movieFilters)
        {
            try
            {
                return TryCountingMovieRecords(movieFilters);
            }
            catch (ArgumentNullException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return 0;
            }
            catch (OverflowException e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
                return 0;
            }
        }

        private int TryCountingMovieRecords(Core.Utilities.Areas.Controllers.MovieFilters movieFilters) => _dbContext.Movies
                    .HandleTitleFiltering(movieFilters)
                    .HandleAvailabilityFilter(movieFilters)
                    .HandleGenreFilter(movieFilters)
                    .Count();

        public void BindMovieRentalRecordWithMovieCopies(MovieRentalRecord movieRentalRecord, MovieCopy[] movieCopies)
        {
            try
            {
                TryBindingMovieRentalRecordWithMovieCopies(movieCopies, movieRentalRecord);
            }
            catch(Exception e)
            {
                var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _loggingService.Writer.Error("{MethodName} -> {Error}", reflectedFunctionName, e.Message);
            }
        }

        private void TryBindingMovieRentalRecordWithMovieCopies(MovieCopy[] movieCopies, MovieRentalRecord movieRentalRecord)
        {
            foreach (var MovieCopy in movieCopies)
            {
                MovieCopy.IsAvailable = false;
                MovieCopy.MovieRentalRecordId = movieRentalRecord.Id;
            }
            BatchUpdateMovieCopies(movieCopies);
        }
    }
}
