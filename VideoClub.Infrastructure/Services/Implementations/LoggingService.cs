﻿using Serilog;
using Serilog.Formatting.Compact;
using System;
using VideoClub.Infrastructure.Services.Interfaces;

namespace VideoClub.Infrastructure.Services.Implementations
{
    public class LoggingService : ILoggingService
    {
        public ILogger Writer => Log.Logger;

        public LoggingService()
        {
            Log.Logger = new LoggerConfiguration()
            .WriteTo.File(
                path: AppDomain.CurrentDomain.BaseDirectory + "Log/logs.txt",
                rollingInterval: RollingInterval.Day,
                shared: true,
                retainedFileCountLimit: 100,
                outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}"
            )
            .WriteTo.File(
                formatter: new CompactJsonFormatter(),
                path: AppDomain.CurrentDomain.BaseDirectory + "Log/logs.json",
                rollingInterval: RollingInterval.Day,
                shared: true,
                retainedFileCountLimit: 100
            ).WriteTo.Console()
            .CreateLogger();
        }
    }
}
