using System.Data.Entity.Migrations;

namespace VideoClub.Infrastructure.Migrations
{
    internal sealed class UserConfiguration : DbMigrationsConfiguration<VideoClub.Infrastructure.Data.ClientUserDbContext>
    {
        public UserConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(VideoClub.Infrastructure.Data.ClientUserDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
