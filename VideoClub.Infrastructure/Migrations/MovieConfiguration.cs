using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace VideoClub.Infrastructure.Migrations
{
    internal sealed class MovieConfiguration : DbMigrationsConfiguration<VideoClub.Infrastructure.Data.MovieDbContext>
    {
        public MovieConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(VideoClub.Infrastructure.Data.MovieDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
