﻿using System.Data.Entity;
using VideoClub.Core.Entities;

namespace VideoClub.Infrastructure.Data
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext() : base("DefaultConnection") { }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieCopy> MovieCopies { get; set; }
    }
}
