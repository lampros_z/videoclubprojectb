﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using VideoClub.Core.Entities;

namespace VideoClub.Infrastructure.Data
{
    public class ClientUserDbContext : IdentityDbContext<ClientUser>
    {
        public ClientUserDbContext() : base("DefaultConnection", false) { }
        public DbSet<MovieRentalRecord> movieRentalRecords { get; set; }
        public DbSet<MovieCopy> movieCopies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var movieGenres = modelBuilder.Entity<MovieGenres>();
            movieGenres.ToTable("MovieGenres");
            movieGenres.HasKey(x => x.Id);


            var clientPreferedGenres = modelBuilder.Entity<UserPreferedGenres>();
            clientPreferedGenres.ToTable("ClientPreferedGenres");
            clientPreferedGenres.HasKey(x => x.Id);

            var movieCopy = modelBuilder.Entity<MovieCopy>();
            movieCopy.ToTable("MovieCopies");
            movieCopy.HasKey(x => x.Id);

            var movie = modelBuilder.Entity<Movie>();
            movie.ToTable("Movies");
            movie.HasKey(x => x.Id);
            movie.HasMany(x => x.MovieCopies).WithOptional().HasForeignKey(x => x.MovieId);
            movie.HasMany(x => x.Genres).WithOptional().HasForeignKey(x => x.MovieId);

            var movieRentalRecord = modelBuilder.Entity<MovieRentalRecord>();
            movieRentalRecord.ToTable("MovieRentalRecords");
            movieRentalRecord.HasKey(x => x.Id);
            movieRentalRecord.HasMany(x => x.MovieCopies).WithOptional().HasForeignKey(x => x.MovieRentalRecordId).WillCascadeOnDelete();

            var user = modelBuilder.Entity<ClientUser>();
            user.ToTable("Users");
            user.Property(x => x.FirstName).HasMaxLength(256);
            user.Property(x => x.LastName).HasMaxLength(256);
            user.Property(x => x.UserName).IsRequired().HasMaxLength(256);
            user.HasKey(x => x.Id);
            user.HasMany(x => x.movieRentalRecords).WithRequired().HasForeignKey(x => x.ClientUserId);
            user.HasMany(x => x.FavouriteMovieGenres).WithRequired().HasForeignKey(x => x.UserId);
        }

    }
}
