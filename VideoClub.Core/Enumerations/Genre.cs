﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoClub.Core.Enumerations
{
    public enum Genre
    {
        None,
        Action,
        Adventure,
        Crime,
        Mystery,
        Comedy,
        Drama,
        Fantasy,
        Historical,
        Horror,
        Philosophical,
        Political,
        Romance
    }
}
