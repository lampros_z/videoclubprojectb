﻿namespace VideoClub.Core.Entities
{
    public class MovieGenres
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public int Genre { get; set; }
    }
}
