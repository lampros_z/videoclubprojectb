﻿using System.Collections.Generic;

namespace VideoClub.Core.Entities
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public virtual ICollection<MovieGenres> Genres { get; set; }
        public virtual ICollection<MovieCopy> MovieCopies { get; set; }

        public Movie()
        {
            Genres = new List<MovieGenres>();
            MovieCopies = new List<MovieCopy>();
        }
    }
}