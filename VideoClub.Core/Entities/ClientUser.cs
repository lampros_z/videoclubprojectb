﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace VideoClub.Core.Entities
{
    public class ClientUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual ICollection<UserPreferedGenres> FavouriteMovieGenres { get; set; }
        public virtual ICollection<MovieRentalRecord> movieRentalRecords { get; set; }

        public ClientUser()
        {
            FavouriteMovieGenres = new List<UserPreferedGenres>();
            movieRentalRecords = new List<MovieRentalRecord>();
        }

        public ClientUser(string FirstName, string LastName, string Email)
        {
            base.UserName = Email;
            this.FirstName = FirstName;
            this.LastName = LastName;
            FavouriteMovieGenres = new List<UserPreferedGenres>();
            movieRentalRecords = new List<MovieRentalRecord>();
        }
    }
}
