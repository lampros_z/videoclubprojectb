﻿namespace VideoClub.Core.Entities
{
    public class MovieCopy
    {
        public int Id { get; set; }
        public bool IsAvailable { get; set; }
        public int? MovieId { get; set; }
        public int? MovieRentalRecordId { get; set; }

        public MovieCopy()
        {
            IsAvailable = true;
        }
    }
    
}
