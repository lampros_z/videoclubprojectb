﻿using System;

namespace VideoClub.Core.Entities
{
    public class UserPreferedGenres
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int Genre { get; set; }

        public UserPreferedGenres()
        {
            UserId = Guid.NewGuid().ToString();
        }
    }
}
