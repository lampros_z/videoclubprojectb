﻿namespace VideoClub.Core.Utilities.Areas.Controllers
{
    public class RentalRecordMovieInfo
    {
        public int MovieId { get; set; }
        public int Quantity { get; set; }
    }
}
