﻿namespace VideoClub.Core.Utilities.Areas.Controllers
{
    public class RentalRecordInfo
    {
        public string UserId { get; set; }
        public string CommentText { get; set; }

        public RentalRecordInfo(string UserId, string CommentText)
        {
            this.UserId = UserId;
            this.CommentText = CommentText;
        }
    }
}
