﻿namespace VideoClub.Core.Utilities.Areas.Controllers
{
    public class PaginationInfo
    {
        public int NumberOfRecordsPerPage { get; private set; }
        public int CurrentPage { get; private set; }
        public int TotalRecords { get; set; }

        public PaginationInfo(int NumberOfClientsPerPage, int? CurrentPage)
        {
            this.NumberOfRecordsPerPage = NumberOfClientsPerPage;
            this.CurrentPage = CurrentPage ?? 1;
        }

        public PaginationInfo(int NumberOfClientsPerPage, int? CurrentPage, int TotalRecords)
        {
            this.NumberOfRecordsPerPage = NumberOfClientsPerPage;
            this.CurrentPage = CurrentPage ?? 1;
            this.TotalRecords = TotalRecords;
        }
    }
}
