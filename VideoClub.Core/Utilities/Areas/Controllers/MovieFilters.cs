﻿using System;

namespace VideoClub.Core.Utilities.Areas.Controllers
{
    public class MovieFilters
    {
        public string TitleSearchValue { get; set; }
        public int[] MovieGenresFilters { get; set; }
        public bool[] AvailabilityFilters { get;  set; }

        public MovieFilters(string TitleSearchValue, int[] MovieGenresFilters, bool[] AvailabilityFilters)
        {
            this.TitleSearchValue = TitleSearchValue;
            this.MovieGenresFilters = MovieGenresFilters;
            this.AvailabilityFilters = AvailabilityFilters;
        }

        public MovieFilters(string TitleSearchValue, string[] MovieGenresFilters, string[] AvailabilityFilters)
        {
            this.TitleSearchValue = TitleSearchValue ?? "";
            try 
            {
                this.MovieGenresFilters = Array.ConvertAll(MovieGenresFilters, int.Parse);
            }
            catch(ArgumentNullException e)
            {
                this.MovieGenresFilters = new int[0];
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            try
            {
                this.AvailabilityFilters = Array.ConvertAll(AvailabilityFilters, bool.Parse);
            }catch(ArgumentNullException e)
            {
                this.AvailabilityFilters = new bool[0];
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }

        public MovieFilters()
        {

        }
    }
}
