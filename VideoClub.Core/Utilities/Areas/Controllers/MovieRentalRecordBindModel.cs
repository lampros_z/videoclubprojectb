﻿namespace VideoClub.Core.Utilities.Areas.Controllers
{
    public class MovieRentalRecordBindModel
    {
        public int? Id { get; set; }
        public int Quantity { get; set; }
    }
}