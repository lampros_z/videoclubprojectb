﻿using System.Linq;
using VideoClub.Core.Entities;
using VideoClub.Core.Utilities.Areas.Controllers;

namespace VideoClub.Core.Extensions
{
    public static class MovieFilters
    {
        public static IQueryable<Movie> HandleAvailabilityFilter(this IQueryable<Movie> query, Utilities.Areas.Controllers.MovieFilters movieInfo)
        {
            if (movieInfo.AvailabilityFilters == null)
                return query;
            foreach (var AvailabilityFilter in movieInfo.AvailabilityFilters)
            {
                if (AvailabilityFilter)
                {
                    query = query.Where(m => m.MovieCopies.Any(mc => mc.IsAvailable == true));
                }
                else
                {
                    query = query.Where(m => m.MovieCopies.All(mc => mc.IsAvailable == false));
                }
            }
            return query;
        }

        public static IQueryable<Movie> HandleGenreFilter(this IQueryable<Movie> query, Utilities.Areas.Controllers.MovieFilters movieInfo)
        {
            if (movieInfo.MovieGenresFilters == null)
                return query;
            foreach (var GnereFilter in movieInfo.MovieGenresFilters)
            {
                query = query.Where(m => m.Genres.Any(g => g.Genre == GnereFilter));
            }
            return query;
        }
        
        public static IQueryable<Movie> HandleTitleFiltering(this IQueryable<Movie> query, Utilities.Areas.Controllers.MovieFilters movieInfo)
        {
            if (movieInfo.TitleSearchValue == null || movieInfo.TitleSearchValue.Trim(' ') == "")
            {
                return query;
            }
            else
            {
                query = query.Where(m => m.Title.ToLower().StartsWith(movieInfo.TitleSearchValue.ToLower()));
                return query;
            }
        }

        public static IQueryable<Movie> HandleTitleFiltering(this IQueryable<Movie> query, string Title)
        {
            if (Title == null || Title.Trim(' ') == "")
            {
                return query;
            }
            else
            {
                query = query.Where(m => m.Title.ToLower().StartsWith(Title.ToLower()));
                return query;
            }
        }

    }
}
