﻿using VideoClub.Core.Entities;
using VideoClub.Core.Utilities.Areas.Controllers;

namespace VideoClub.Core.Interfaces
{
    public interface IUserService
    {
        ClientUser[] FetchClientUsers(PaginationInfo paginationInfo);
        int GetTotalNumberOfClientUsers();
        MovieRentalRecord[] FetchClientRentalRecordHistory(string userId, PaginationInfo paginationInfo);
        MovieRentalRecord[] FetchClientActiveRentalRecords(string userId, PaginationInfo paginationInfo);
        MovieRentalRecord[] FetchAllActiveRentalRecords(PaginationInfo paginationInfo);
        int CountAllActiveRentalRecords();
        int CountClientActiveRentalRecords(string userId);
        int CountUserRentalRecords(string userId);
        MovieRentalRecord[] FetchUserActiveRentalRecords(string userId, PaginationInfo paginationInfo);
        ClientUser FindClientUserById(string id);
        ClientUser FindClientUserByUsername(string userName);
        MovieRentalRecord RegisterUsersMovieRentalRecord(RentalRecordInfo info);
        void CreateUser(ClientUser clientUser);
        void UpdateUserRoles(ClientUser clientUser);
        void UpdateUserRentalRecords(ClientUser clientUser);
    }
}
