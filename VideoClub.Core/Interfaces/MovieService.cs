﻿using VideoClub.Core.Entities;
using VideoClub.Core.Utilities.Areas.Controllers;

namespace VideoClub.Core.Interfaces
{
    public interface IMovieService
    {
        Movie[] FetchMovies(MovieFilters movieFilters, PaginationInfo paginationInfo);
        int CountMovieRecords(MovieFilters movieFilters);
        void AddMovie(Movie movie);
        Movie[] GetMoviesWithAvailableMovieCopiesByTitle(string title);
        MovieCopy[] FetchMovieCopies(MovieRentalRecordBindModel[] movie);
        void UpdateMovieCopy(MovieCopy movieCopy);
        void BatchUpdateMovieCopies(MovieCopy[] movieCopies);
        void BindMovieRentalRecordWithMovieCopies(MovieRentalRecord movieRentalRecord, MovieCopy[] movieCopies);
    }
}
