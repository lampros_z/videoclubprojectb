﻿using AutoMapper;
using VideoClub.Core.Entities;
using VideoClub.Web.CustomValueResolvers;
using VideoClub.Web.Models.View;

namespace VideoClub.Web.Profiles.Admin
{
    public class ClientUserProfiles : Profile
    {
        public ClientUserProfiles()
        {
            CreateMap<ClientUser, ClientUserViewModel>()
                .ForMember(destination => destination.RentalRecordsCount, option => option.MapFrom<ClientActiveRentalRecordValueResolver>());
            CreateMap<MovieRentalRecord, ClientRentalRecordViewModel>()
                .ForMember(destination => destination.Username, option => option.MapFrom<ClientRentalRecordHistoryValueResolver>())
                .ForMember(destination => destination.RentalRecordId, option => option.MapFrom(source => source.Id));
        }
    }
}