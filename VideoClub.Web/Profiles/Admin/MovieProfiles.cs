﻿using AutoMapper;
using VideoClub.Core.Entities;
using VideoClub.Web.Models.Bind;
using VideoClub.Web.CustomValueResolvers;
using VideoClub.Web.Models.View;

namespace VideoClub.Web.Profiles.Admin
{
    public class MovieProfiles : Profile
    {
        public MovieProfiles()
        {
            CreateMap<MovieBindModel, Movie>()
                .ForMember(destination => destination.Genres, options => options.MapFrom<GenreValueResolver>())
                .ForMember(destination => destination.MovieCopies, options => options.MapFrom<MovieCopyValueResolver>());
            CreateMap<Movie, MovieDataViewModel>()
                .ForMember(destination => destination.GenreString, options => options.MapFrom<GenreStringValueResolver>())
                .ForMember(destination => destination.AvailableMovieCopies, options => options.MapFrom<AvailableMovieCopiesValueResolver>());
            CreateMap<Movie, MovieListViewModel>()
                .ForMember(destination => destination.MovieId, options => options.MapFrom(source => source.Id))
                .ForMember(destination => destination.Available, options => options.MapFrom<AvailableCopiesValueResolver>());
        }
    }
}