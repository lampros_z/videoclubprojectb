﻿namespace VideoClub.Web.Models.Bind
{
    public class MovieBindModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public int MovieCopies { get; set; }
        public bool ActionChecked { get; set; }
        public bool AdventureChecked { get; set; }
        public bool CrimeChecked { get; set; }
        public bool MysteryChecked { get; set; }
        public bool ComedyChecked { get; set; }
        public bool DramaChecked { get; set; }
        public bool FantasyChecked { get; set; }
        public bool HistoricalChecked { get; set; }
        public bool HorrorChecked { get; set; }
        public bool PhilosophicalChecked { get; set; }
        public bool PoliticalChecked { get; set; }
        public bool RomanceChecked { get; set; }

    }
}