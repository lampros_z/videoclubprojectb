﻿using System.ComponentModel.DataAnnotations;

namespace VideoClub.Web.Models.Bind
{
    public class UserBindModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        [Display(Name = "Last name")]
        public string LastName { get; set; }
        public bool ActionChecked { get; set; }
        public bool AdventureChecked { get; set; }
        public bool CrimeChecked { get; set; }
        public bool MysteryChecked { get; set; }
        public bool ComedyChecked { get; set; }
        public bool DramaChecked { get; set; }
        public bool FantasyChecked { get; set; }
        public bool HistoricalChecked { get; set; }
        public bool HorrorChecked { get; set; }
        public bool PhilosophicalChecked { get; set; }
        public bool PoliticalChecked { get; set; }
        public bool RomanceChecked {get; set;}
    }
}