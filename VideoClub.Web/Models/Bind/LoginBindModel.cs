﻿namespace VideoClub.Web.Models.Bind
{
    public class LoginBindModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}