﻿using System.ComponentModel.DataAnnotations;

namespace VideoClub.Web.Models.View
{
    public class ClientUserViewModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        [Display(Name = "Last name")]
        public string LastName { get; set; }
        [Display(Name = "Rental records")]
        public int RentalRecordsCount { get; set; }
    }
}