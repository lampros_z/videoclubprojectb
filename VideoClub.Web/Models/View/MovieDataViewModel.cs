﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VideoClub.Core.Entities;
using VideoClub.Core.Enumerations;

namespace VideoClub.Web.Models.View
{
    public class MovieDataViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string YoutubeUrl { get; set; }
        [Display(Name = "Genre")]
        public string GenreString { get; set; }
        public int AvailableMovieCopies { get; set; }

    }
}