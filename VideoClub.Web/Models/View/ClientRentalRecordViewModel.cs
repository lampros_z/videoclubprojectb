﻿using AutoMapper;
using AutoMapper.Configuration.Annotations;
using System;
using VideoClub.Core.Entities;

namespace VideoClub.Web.Models.View
{
    public class ClientRentalRecordViewModel
    {
        public string ClientUserId { get; set; }
        public string Username { get; set; }
        public int RentalRecordId { get; set; }
        public DateTime RentDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public bool Returned { get; set; }
        public string Comment { get; set; }
    }
}