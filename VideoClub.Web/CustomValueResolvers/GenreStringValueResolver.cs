﻿using AutoMapper;
using VideoClub.Core.Entities;
using VideoClub.Web.Models.View;
using VideoClub.Web.UtilityClasses;
namespace VideoClub.Web.CustomValueResolvers
{
    public class GenreStringValueResolver : IValueResolver<Movie, MovieDataViewModel, string>
    {
        public string Resolve(Movie source, MovieDataViewModel destination, string destMember, ResolutionContext context)
        {
            return AdminControllerUtils.TransformGenresToString(source.Genres);
        }
    }
}