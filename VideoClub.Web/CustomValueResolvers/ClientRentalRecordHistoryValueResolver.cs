﻿using AutoMapper;
using VideoClub.Core.Entities;
using VideoClub.Web.Models.View;

namespace VideoClub.Web.CustomValueResolvers
{
    public class ClientRentalRecordHistoryValueResolver : IValueResolver<MovieRentalRecord, ClientRentalRecordViewModel, string>
    {
        public string Resolve(MovieRentalRecord source, ClientRentalRecordViewModel destination, string destMember, ResolutionContext context)
        {
            return source.clientUser.UserName;
        }
    }
}