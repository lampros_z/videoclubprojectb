﻿using AutoMapper;
using System.Linq;
using VideoClub.Core.Entities;
using VideoClub.Web.Models.View;

namespace VideoClub.Web.CustomValueResolvers
{
    public class AvailableCopiesValueResolver : IValueResolver<Movie, MovieListViewModel, int>
    {
        public int Resolve(Movie source, MovieListViewModel destination, int destMember, ResolutionContext context)
        {
            return source.MovieCopies.Count(mc => mc.IsAvailable);
        }
    }
}