﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VideoClub.Core.Entities;
using VideoClub.Web.Models.View;

namespace VideoClub.Web.CustomValueResolvers
{
    public class AvailableMovieCopiesValueResolver : IValueResolver<Movie, MovieDataViewModel, int>
    {
        public int Resolve(Movie source, MovieDataViewModel destination, int destMember, ResolutionContext context)
        {
            return source.MovieCopies.Count(mc => mc.IsAvailable);
        }
    }
}