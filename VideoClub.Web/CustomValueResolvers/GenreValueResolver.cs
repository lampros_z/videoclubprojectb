using AutoMapper;
using System.Collections.Generic;
using VideoClub.Core.Entities;
using VideoClub.Web.Models.Bind;
using VideoClub.Web.UtilityClasses;

namespace VideoClub.Web.CustomValueResolvers
{
    public class GenreValueResolver : IValueResolver<MovieBindModel, Movie, ICollection<MovieGenres>>
    {
        public ICollection<MovieGenres> Resolve(MovieBindModel source, Movie destination, ICollection<MovieGenres> destMember, ResolutionContext context)
        {
            return MovieControllerUtils.HandleMovieGenre(source);
        }
    }
}