﻿using AutoMapper;
using System.Collections.Generic;
using VideoClub.Core.Entities;
using VideoClub.Web.Models.Bind;
using VideoClub.Web.UtilityClasses;
namespace VideoClub.Web.CustomValueResolvers
{
    public class MovieCopyValueResolver : IValueResolver<MovieBindModel, Movie, ICollection<MovieCopy>>
    {
        public ICollection<MovieCopy> Resolve(MovieBindModel source, Movie destination, ICollection<MovieCopy> destMember, ResolutionContext context)
        {
            return AdminControllerUtils.GenerateMovieCopies(source);
        }
    }
}