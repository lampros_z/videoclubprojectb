﻿using AutoMapper;
using System.Linq;
using VideoClub.Core.Entities;
using VideoClub.Web.Models.View;

namespace VideoClub.Web.CustomValueResolvers
{
    public class ClientActiveRentalRecordValueResolver : IValueResolver<ClientUser, ClientUserViewModel, int>
    {
        public int Resolve(ClientUser source, ClientUserViewModel destination, int destMember, ResolutionContext context)
        {
            return source.movieRentalRecords.Count(m => m.Returned == false);
        }
    }
}