﻿using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using System.Web.Mvc;
using VideoClub.Common.Services;
using VideoClub.Core.Interfaces;
using VideoClub.Infrastructure.Data;
using VideoClub.Infrastructure.Services.Implementations;
using VideoClub.Infrastructure.Services.Interfaces;

namespace VideoClub.Web
{
    public class ContainerConfig : Module
    {
        public static void RegisterContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterType<ClientUserDbContext>().As<ClientUserDbContext>().InstancePerRequest();
            builder.RegisterType<MovieDbContext>().As<MovieDbContext>().InstancePerRequest();
            builder.RegisterType<UserServiceImplementation>().As<IUserService>().InstancePerRequest();
            builder.RegisterType<MovieServiceImplementation>().As<IMovieService>().InstancePerRequest();
            builder.RegisterType<LoggingService>().As<ILoggingService>().InstancePerRequest();
            builder.AddAutoMapper(typeof(MvcApplication).Assembly);
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}