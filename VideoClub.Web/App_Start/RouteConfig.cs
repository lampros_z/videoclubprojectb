﻿using System.Web.Mvc;
using System.Web.Routing;

namespace VideoClub.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            /*routes.MapRoute(
                name: "AdminRedirectToRentMovie",
                url: "Admin/RentMovie/{id}",
                new { controller = "Movie", action = "RentMovie", id = UrlParameter.Optional },
                namespaces: new string[] { "VideoClub.Web.Controllers" }
            );

            routes.MapRoute(
                name: "AdminRedirectToSearchMovie",
                url: "Admin/SearchMovie/",
                new { controller = "Movie", action = "SearchMovie" },
                namespaces: new string[] { "VideoClub.Web.Controllers" }
            );*/

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "VideoClub.Web.Controllers" }
            );
            
        }
    }
}
