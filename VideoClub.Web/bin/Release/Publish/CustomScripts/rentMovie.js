﻿var seacrhbar = document.getElementById("searchbar");
var MovieList = document.getElementById("movie-list");
var MovieTable = document.getElementById("rent-table-body");
var CommentTextArea = document.getElementById("text-area-id");
var MovieMap = new Map();

/*var groupBy = (array, key) => {
    return array
        .reduce((accumulator, object) => {
        (accumulator[object[key]] = accumulator[object[key]] || []).push(object);
        return accumulator;
    }, {});
};*/

seacrhbar.addEventListener("keypress", function (event) {
    if (event.keyCode === 13)
        search();
});


function search() {
    let searchText = seacrhbar.value;
    if (searchText == null || searchText == undefined || searchText == "")
        return;
    let url = location.protocol;
    url += "//"
    url += location.host;
    url += "/movies/Home/"
    url += "SearchMovie?Title="
    url += searchText;
   
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            MovieList.innerHTML = "";
            let res = JSON.parse(request.responseText);
            let li;
            MovieMap.clear();
            for (let item of res) {
                if (item.Available == 0)
                    continue;
                li = document.createElement("LI");
                li.setAttribute("id", item.MovieId);
                li.appendChild(document.createTextNode(item.Title + " -- " + item.Available));
                MovieList.appendChild(li);
                MovieMap.set(parseInt(item.MovieId), { "Title" : item.Title, "Availability" : item.Available });
            }
            init();
            setListeners();
        }
    }
    request.open("GET", url);
    request.send(null);
}

function setListeners() {
    MovieList.childNodes.forEach(item => {
        item.addEventListener('click', event => {
            updateTable(item.id);
        })
    })
}

function updateTable(id) {
    let tr = document.createElement("TR");
    let quantity;
    if (sessionStorage.getItem(id) != null && document.getElementById("data" + id) != null) {
        quantity = parseInt(sessionStorage.getItem(id));
        quantity++;
        sessionStorage.setItem(id, quantity);
        document.getElementById("data" + id).childNodes[0].nodeValue = quantity;

    } else {
        if (document.getElementById("data" + id) == null) {
            const uuid = sessionStorage.getItem("uuid");
            sessionStorage.clear();
            sessionStorage.setItem("uuid", uuid);
        }
        quantity = 1;
        sessionStorage.setItem(id, quantity);

        if (MovieMap.has(parseInt(id))) {
            let textNodeId = document.createTextNode(id);
            let tdId = document.createElement("TD");
            tdId.appendChild(textNodeId);
            let textNodeTitle = document.createTextNode(MovieMap.get(parseInt(id)).Title);
            let tdTitle = document.createElement("TD");
            tdTitle.appendChild(textNodeTitle);
            let textNodeQuantity = document.createTextNode(quantity);
            let tdQuantity = document.createElement("TD");
            tdQuantity.setAttribute("id", "data" + id);
            tdQuantity.appendChild(textNodeQuantity);
            let tdAction = document.createElement("TD");
            let iIncr = document.createElement("I");
            iIncr.setAttribute("class", "far fa-plus-square");
            iIncr.setAttribute("onclick", "increaseQuantity(this);");
            let iDecr = document.createElement("I");
            iDecr.setAttribute("class", "far fa-minus-square");
            iDecr.setAttribute("onclick", "deccreaseQuantity(this);");
            let iDelete = document.createElement("I");
            iDelete.setAttribute("class", "fas fa-trash");
            iDelete.setAttribute("onclick", "removeRow(this);");
            tdAction.appendChild(iIncr);
            tdAction.appendChild(iDecr);
            tdAction.appendChild(iDelete);
            tr.appendChild(tdId);
            tr.appendChild(tdTitle);
            tr.appendChild(tdQuantity);
            tr.appendChild(tdAction);
            tr.setAttribute("id",id);
        }
    }
    MovieTable.appendChild(tr);
}

function increaseQuantity(item) {
    let row = item.parentNode.parentNode;
    let id = row.getAttribute("id");
    let max = MovieMap.get(parseInt(id)).Availability;
    let dId = "data" + id;
    if (document.getElementById(dId) != null) {
        let quantityTextNode = document.getElementById(dId).childNodes[0];
        let value = parseInt(quantityTextNode.nodeValue);
        value++;
        if (value <= max)
        quantityTextNode.nodeValue = value;
    }
}

function deccreaseQuantity(item) {
    let row = item.parentNode.parentNode;
    let id = row.getAttribute("id");
    let dId = "data" + id;
    if (document.getElementById(dId) != null) {
        let quantityTextNode = document.getElementById(dId).childNodes[0];
        let value = parseInt(quantityTextNode.nodeValue);
        if (value > 1)
            value--;
        quantityTextNode.nodeValue = value;
    }
}

function removeRow(item) {
    let row = item.parentNode.parentNode;
    sessionStorage.removeItem(row.getAttribute("id"));
    row.parentNode.removeChild(row);
}

function submitMovieRentalRecord(btn) {
    var request = new XMLHttpRequest();
    let url = location.protocol;
    url += "//"
    url += location.host;
    url += "/movies/Home/"
    url += "RentMovie?UserId="
    url += sessionStorage.getItem("uuid");
    url += "&CommentText=" + CommentTextArea.value;
    //parse table
    console.log()
    let movies = parseTable();
    //table to json
    let json = JSON.stringify(movies);
    console.log(json);
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            let response = JSON.parse(request.responseText);
            console.log(response);
            let textNode;
            let pNode;
            let modalDivNode = document.getElementById("result-modal");
            for (let item of response) {
                textNode = document.createTextNode("Movie copy id: " + item.Id);
                pNode = document.createElement("P");
                pNode.appendChild(textNode);
                modalDivNode.appendChild(pNode);
            }
            btn.setAttribute("href", "#open-modal");
            btn.removeAttribute("onclick");
            btn.click();
        }
    }
    request.open("POST", url);
    request.setRequestHeader('Content-Type', 'application/json'); //Use JSON format
    request.send(json);
}

function parseTable() {
    let movies = [];
    for (let item of MovieTable.childNodes) {
        let movieData = {};
        if (item.childNodes[0] == undefined)
            continue;
        movieData["Id"] = item.childNodes[0].innerText;
        movieData["Quantity"] = item.childNodes[2].innerText;
        movies.push(movieData);
    }
    return movies;
}