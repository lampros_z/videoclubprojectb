﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;
using VideoClub.Core.Entities;
using VideoClub.Core.Enumerations;
using VideoClub.Web.Infrastructure;
using VideoClub.Web.Models.Bind;

namespace VideoClub.Web.UtilityClasses
{
    public class UserControllerUtils
    {
        public static void HandleUserFavouriteMovieGenre(ClientUser user, UserBindModel model)
        {
            if (model.ActionChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Action });
            if (model.AdventureChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Adventure });
            if (model.CrimeChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Crime });
            if (model.MysteryChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Mystery });
            if (model.ComedyChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Comedy });
            if (model.DramaChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Drama });
            if (model.FantasyChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Fantasy });
            if (model.HistoricalChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Historical });
            if (model.HorrorChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Horror });
            if (model.PhilosophicalChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Philosophical });
            if (model.PoliticalChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Political });
            if (model.RomanceChecked)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.Romance });
            if (user.FavouriteMovieGenres.Count == 0)
                user.FavouriteMovieGenres.Add(new UserPreferedGenres { Genre = (int)Genre.None });

        }

        public static async Task<IdentityRole> GetUserRoleAsync(ApplicationRoleManager roleManager)
        {
            var role = await roleManager.FindByNameAsync("Client");
            if (role == null)
            {
                var identityResult = await roleManager.CreateAsync(new IdentityRole { Name = "Client" });
                if (identityResult.Succeeded)
                {
                    role = await roleManager.FindByNameAsync("Client");
                    return role;
                }
                return null;
            }
            return role;
        }
    }
}