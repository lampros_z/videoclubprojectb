﻿using System.Web.Mvc;
using VideoClub.Core.Utilities.Areas.Controllers;
using VideoClub.Core.Entities;
using VideoClub.Web.Models.Bind;
using static VideoClub.Web.LocalStrings.AdminLocalStrings;
using System.Collections.Generic;
using VideoClub.Core.Enumerations;
using System;

namespace VideoClub.Web.UtilityClasses
{
    public class AdminControllerUtils
    {
        private static int CalculatePagination(int totalRecords, int recordsPerPage)
        {
            if (recordsPerPage == 0)
                return 0;
            var result = totalRecords / recordsPerPage;
            if (totalRecords % recordsPerPage != 0)
                result += 1;
            return result;
        }

        public static void SetPaginationViewData(ViewDataDictionary viewData, PaginationInfo paginationInfo)
        {
            viewData[PAGINATION_LENGTH_KEY] = CalculatePagination(paginationInfo.TotalRecords, paginationInfo.NumberOfRecordsPerPage);
            viewData[PAGINATION_CURRENT_PAGE_KEY] = paginationInfo.CurrentPage;
        }

        public static void SetPaginationURLSearchTitle(ViewDataDictionary viewData, MovieFilters movieFilters)
        {
            if (movieFilters.TitleSearchValue != null)
                viewData[PAGINATION_URL_SEARCH_TITLE] = Uri.EscapeDataString(movieFilters.TitleSearchValue);
            else
                viewData[PAGINATION_URL_SEARCH_TITLE] = "";
        }

        public static ICollection<MovieCopy> GenerateMovieCopies(MovieBindModel model)
        {
            var MovieCopies = new List<MovieCopy>();
            for (var i = 0; i < model.MovieCopies; i++)
                MovieCopies.Add(new MovieCopy());
            return MovieCopies;
        }

        public static string TransformGenresToString(ICollection<MovieGenres> genres)
        {
            var GenreString = "";
            foreach (var MovieGenre in genres)
            {
                switch (MovieGenre.Genre)
                {
                    case (int)Genre.Action:
                        GenreString += "Action, ";
                        break;
                    case (int)Genre.Adventure:
                        GenreString += "Adventure, ";
                        break;
                    case (int)Genre.Crime:
                        GenreString += "Crime, ";
                        break;
                    case (int)Genre.Mystery:
                        GenreString += "Mystery, ";
                        break;
                    case (int)Genre.Comedy:
                        GenreString += "Comedy, ";
                        break;
                    case (int)Genre.Drama:
                        GenreString += "Drama, ";
                        break;
                    case (int)Genre.Fantasy:
                        GenreString += "Fantasy, ";
                        break;
                    case (int)Genre.Historical:
                        GenreString += "Historical, ";
                        break;
                    case (int)Genre.Horror:
                        GenreString += "Horror, ";
                        break;
                    case (int)Genre.Philosophical:
                        GenreString += "Philosophical, ";
                        break;
                    case (int)Genre.Political:
                        GenreString += "Political, ";
                        break;
                    case (int)Genre.Romance:
                        GenreString += "Romance, ";
                        break;
                    case (int)Genre.None:
                        GenreString += "";
                        break;
                }
            }
            if (GenreString.Length > 2)
                GenreString = GenreString.Substring(0, GenreString.Length - 2);
            return GenreString;
        }
     
    }
}