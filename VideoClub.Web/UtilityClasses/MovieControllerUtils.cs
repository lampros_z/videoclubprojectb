using System.Collections.Generic;
using VideoClub.Core.Entities;
using VideoClub.Core.Enumerations;
using VideoClub.Web.Models.Bind;

namespace VideoClub.Web.UtilityClasses
{
	public class MovieControllerUtils
	{
		public static void HandleMovieGenre(Movie movie, MovieBindModel model)
		{
			if (model.ActionChecked)
				movie.Genres.Add(new MovieGenres { Genre = (int)Genre.Action });
			if (model.AdventureChecked)
				movie.Genres.Add(new MovieGenres { Genre = (int)Genre.Adventure });
			if (model.CrimeChecked)
				movie.Genres.Add(new MovieGenres { Genre = (int)Genre.Crime });
			if (model.MysteryChecked)
				movie.Genres.Add(new MovieGenres { Genre = (int)Genre.Mystery });
			if (model.ComedyChecked)
				movie.Genres.Add(new MovieGenres { Genre = (int)Genre.Comedy });
			if (model.DramaChecked)
				movie.Genres.Add(new MovieGenres { Genre = (int)Genre.Drama });
			if (model.FantasyChecked)
				movie.Genres.Add(new MovieGenres { Genre = (int)Genre.Fantasy });
			if (model.HistoricalChecked)
				movie.Genres.Add(new MovieGenres { Genre = (int)Genre.Historical });
			if (model.HorrorChecked)
				movie.Genres.Add(new MovieGenres { Genre = (int)Genre.Horror });
			if (model.PhilosophicalChecked)
				movie.Genres.Add(new MovieGenres { Genre = (int)Genre.Philosophical });
			if (model.PoliticalChecked)
				movie.Genres.Add(new MovieGenres { Genre = (int)Genre.Political });
			if (model.RomanceChecked)
				movie.Genres.Add(new MovieGenres { Genre = (int)Genre.Romance });
			if (movie.Genres.Count == 0)
				movie.Genres.Add(new MovieGenres { Genre = (int)Genre.None });
		}

		public static ICollection<MovieGenres> HandleMovieGenre(MovieBindModel model)
		{
			var MovieGenres = new List<MovieGenres>();
			if (model.ActionChecked)
				MovieGenres.Add(new MovieGenres { Genre = (int)Genre.Action });
			if (model.AdventureChecked)
				MovieGenres.Add(new MovieGenres { Genre = (int)Genre.Adventure });
			if (model.CrimeChecked)
				MovieGenres.Add(new MovieGenres { Genre = (int)Genre.Crime });
			if (model.MysteryChecked)
				MovieGenres.Add(new MovieGenres { Genre = (int)Genre.Mystery });
			if (model.ComedyChecked)
				MovieGenres.Add(new MovieGenres { Genre = (int)Genre.Comedy });
			if (model.DramaChecked)
				MovieGenres.Add(new MovieGenres { Genre = (int)Genre.Drama });
			if (model.FantasyChecked)
				MovieGenres.Add(new MovieGenres { Genre = (int)Genre.Fantasy });
			if (model.HistoricalChecked)
				MovieGenres.Add(new MovieGenres { Genre = (int)Genre.Historical });
			if (model.HorrorChecked)
				MovieGenres.Add(new MovieGenres { Genre = (int)Genre.Horror });
			if (model.PhilosophicalChecked)
				MovieGenres.Add(new MovieGenres { Genre = (int)Genre.Philosophical });
			if (model.PoliticalChecked)
				MovieGenres.Add(new MovieGenres { Genre = (int)Genre.Political });
			if (model.RomanceChecked)
				MovieGenres.Add(new MovieGenres { Genre = (int)Genre.Romance });
			if (MovieGenres.Count == 0)
				MovieGenres.Add(new MovieGenres { Genre = (int)Genre.None });
			return MovieGenres;
		}

	}
}