﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using VideoClub.Core.Entities;
using VideoClub.Infrastructure.Data;
using VideoClub.Web.Infrastructure;

[assembly: OwinStartup(typeof(VideoClub.Web.Startup))]

namespace VideoClub.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext(() => new ClientUserDbContext());
            app.CreatePerOwinContext<UserStore<ClientUser>>((opt, cont) => new UserStore<ClientUser>(cont.Get<ClientUserDbContext>()));
            app.CreatePerOwinContext<UserManager<ClientUser>>(
                (opt, cont) => new UserManager<ClientUser>(cont.Get<UserStore<ClientUser>>()));

            app.CreatePerOwinContext<SignInManager<ClientUser, string>>(
                (opt, cont) =>
                new SignInManager<ClientUser, string>(cont.Get<UserManager<ClientUser>>(),
                cont.Authentication)

            );

            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie
            }
            );
        }
    }
}
