﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Configuration;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using VideoClub.Core.Entities;
using VideoClub.Infrastructure.Data;

namespace VideoClub.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //Autofac Init
            ContainerConfig.RegisterContainer();
            //Create default admin account
            InitAdmin();
        }
        private void InitAdmin()
        {
            /*Fetch admin data from configuration file*/
            var Username = ConfigurationManager.AppSettings["AdminUsername"];
            var Password = ConfigurationManager.AppSettings["AdminPassword"];
            var FirstName = ConfigurationManager.AppSettings["FirstName"];
            var LastName = ConfigurationManager.AppSettings["LastName"];

            /*Initialize user store and user manager from Microsoft.Owin.Identity*/
            string connectionString = ConfigurationManager.AppSettings["dbConnection"];
            var dbContext = new ClientUserDbContext();
            var userStore = new UserStore<ClientUser>(dbContext);
            var userManager = new UserManager<ClientUser>(userStore);
            /*If admin account does not exist create*/
            var foundUser = userManager.FindByName(Username);
            if (foundUser == null)
            {
                var result = userManager.Create(new ClientUser(FirstName, LastName, Username), Password);
                if (!result.Succeeded)
                    throw new Exception("Invalid Initialization");
                var role = new IdentityRole { Name = "Admin" };
                /*Initialize role store and role manager from Microsoft.Owin.Identity*/
                var roleStore = new RoleStore<IdentityRole>();
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                /*If role Admin does not exist create*/
                if (!roleManager.RoleExists(role.Name))
                {
                    var identityResult = roleManager.Create(role);
                    if (!identityResult.Succeeded)
                        throw new Exception("Invalid Initialization");
                }
                //find user and fecth UserId
                var clientUser = userManager.FindByName(Username);
                //find role and fetch RoloId
                var clientRole = roleManager.FindByName(role.Name);
                //create IdentityUserRole instance
                var IdUserRole = new IdentityUserRole { UserId = clientUser.Id, RoleId = clientRole.Id };
                //assign role to user
                clientUser.Roles.Add(IdUserRole);
                //update user
                var identityResult1 = userManager.Update(clientUser);
                if (!identityResult1.Succeeded)
                    throw new Exception("Invalid Initialization");
            }
        }
    }
}
