﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VideoClub.Core.Entities;
using VideoClub.Core.Interfaces;
using VideoClub.Core.Utilities.Areas.Controllers;
using VideoClub.Web.CustomBinders;
using VideoClub.Web.Models.Bind;
using VideoClub.Web.Models.View;
using VideoClub.Web.UtilityClasses;
using static VideoClub.Web.LocalStrings.AdminLocalStrings;
namespace VideoClub.Web.Areas.admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class HomeController : Controller
    {
        // GET: admin/Home

        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;


        public HomeController(IUserService userService, IMovieService movieService, IMapper mapper)
        {
            _userService = userService;
            _movieService = movieService;
            _mapper = mapper;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListClients(int? pageNo)
        {
            var totalNumberOfClientUsers = _userService.GetTotalNumberOfClientUsers();
            var paginationInformation = new PaginationInfo(NUMBER_OF_CLIENTS_PER_PAGE, pageNo, totalNumberOfClientUsers);
            var clientUsers = _userService.FetchClientUsers(paginationInformation);
            var clientUsersViewModel = _mapper.Map<List<ClientUserViewModel>>(clientUsers);
            AdminControllerUtils.SetPaginationViewData(ViewData, paginationInformation);
            return View(clientUsersViewModel);
        }

        public ActionResult ListRentalRecordsHistory(string id, int? pageNo)
        {
            var totalMovieRentalRecords = _userService.CountUserRentalRecords(id);
            var paginationInformation = new PaginationInfo(NUMBER_OF_RENTAL_RECORDS_PER_PAGE, pageNo, totalMovieRentalRecords);
            var rentalRecords = _userService.FetchClientRentalRecordHistory(id, paginationInformation);
            var viewModel = _mapper.Map<List<ClientRentalRecordViewModel>>(rentalRecords);
            AdminControllerUtils.SetPaginationViewData(ViewData, paginationInformation);
            return View(viewModel);
        }

        public ActionResult ListActiveRentalRecords(int? pageNo)
        {
            var totalActiveRentalRecords = _userService.CountAllActiveRentalRecords();
            var paginationInformation = new PaginationInfo(NUMBER_OF_ACTIVE_RENTAL_HISTORY_RECORDS_PER_PAGE, pageNo, totalActiveRentalRecords);
            var movieRentalRecords = _userService.FetchAllActiveRentalRecords(paginationInformation);
            var viewModel = _mapper.Map<List<ClientRentalRecordViewModel>>(movieRentalRecords);
            AdminControllerUtils.SetPaginationViewData(ViewData, paginationInformation);
            return View(viewModel);
        }

        public ActionResult ListActiveRentalRecordsHistory(string id, int? pageNo)
        {
            var totalClientActiveRentalRecords = _userService.CountClientActiveRentalRecords(id);
            var paginationInformation = new PaginationInfo(NUMBER_OF_ACTIVE_RENTAL_HISTORY_RECORDS_PER_PAGE, pageNo, totalClientActiveRentalRecords);
            var rentalRecords = _userService.FetchClientActiveRentalRecords(id, paginationInformation);
            var viewModel = _mapper.Map<List<ClientRentalRecordViewModel>>(rentalRecords);
            AdminControllerUtils.SetPaginationViewData(ViewData, paginationInformation);
            return View(viewModel);
        }

        public ActionResult ViewSettings()
        {
            return View();
        }

        public ActionResult AddMovie()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddMovie(MovieBindModel model)
        {
            var movie = new Movie();
            _mapper.Map(model, movie);
            _movieService.AddMovie(movie);
            return View();
        }

        public ActionResult Movies(int? pageNo, [ModelBinder(typeof(MovieCustomBinder))]MovieFilters movieFilters)
        {
            var totalMovies = _movieService.CountMovieRecords(movieFilters);
            var paginationInformation = new PaginationInfo(NUMBER_OF_MOVIES_PER_PAGE, pageNo, totalMovies);
            var movies = _movieService.FetchMovies(movieFilters, paginationInformation);
            var viewModel = _mapper.Map<List<MovieDataViewModel>>(movies);
            AdminControllerUtils.SetPaginationViewData(ViewData, paginationInformation);
            AdminControllerUtils.SetPaginationURLSearchTitle(ViewData, movieFilters);
            return View(viewModel);
        }

    }
}
