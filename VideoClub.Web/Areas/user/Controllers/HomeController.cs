﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VideoClub.Core.Entities;
using VideoClub.Core.Interfaces;
using VideoClub.Core.Utilities.Areas.Controllers;
using VideoClub.Infrastructure.Services.Interfaces;
using VideoClub.Web.CustomBinders;
using VideoClub.Web.Infrastructure;
using VideoClub.Web.Models.Bind;
using VideoClub.Web.Models.View;
using VideoClub.Web.UtilityClasses;
using static VideoClub.Web.LocalStrings.AdminLocalStrings;

namespace VideoClub.Web.Areas.user.Controllers
{
    public class HomeController : Controller
    {
        // GET: user/Home
        private UserManager<ClientUser> UserManager => HttpContext.GetOwinContext().Get<UserManager<ClientUser>>();
        private SignInManager<ClientUser, string> SignInManager => HttpContext.GetOwinContext().Get<SignInManager<ClientUser, string>>();
        private ApplicationRoleManager AppRoleManager => Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;
        private readonly ILoggingService _loggingService;

        public HomeController(IUserService userService, IMovieService movieService, IMapper mapper, ILoggingService loggingService)
        {
            _userService = userService;
            _movieService = movieService;
            _mapper = mapper;
            _loggingService = loggingService;
        }

        [Authorize(Roles = "Client")]
        public ActionResult Index()
        {
            var username = SignInManager.AuthenticationManager.User.Identity.Name;
            var reflectedFunctionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _loggingService.Writer.Information(reflectedFunctionName+" : " + username);
            var clientUser = _userService.FindClientUserByUsername(username);
            return View(clientUser);
        }

        [HttpGet]
        public ActionResult Register()
        {
            return Index();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(UserBindModel model)
        {
            var identityUser = await UserManager.FindByNameAsync(model.Email);
            if (identityUser != null)
            {
                return RedirectToAction("Index", "Home");
            }


            var user = new ClientUser(model.FirstName, model.LastName, model.Email);

            UserControllerUtils.HandleUserFavouriteMovieGenre(user, model);

            var identityResult = await UserManager.CreateAsync(user, model.Password);

            if (!identityResult.Succeeded)
            {
                foreach (var err in identityResult.Errors)
                    System.Diagnostics.Debug.WriteLine(err);
                ModelState.AddModelError("", identityResult.Errors.FirstOrDefault());
                return View(model);
            }

            var identityRole = await UserControllerUtils.GetUserRoleAsync(AppRoleManager);
            identityUser = await UserManager.FindByNameAsync(model.Email);
            var idRole = new IdentityUserRole { UserId = identityUser.Id, RoleId = identityRole.Id };
            user.Roles.Add(idRole);
            IdentityResult identityResult1 = await UserManager.UpdateAsync(user);
            if (!identityResult1.Succeeded)
            {
                foreach (var err in identityResult.Errors)
                    System.Diagnostics.Debug.WriteLine(err);
                ModelState.AddModelError("", identityResult.Errors.FirstOrDefault());
                return View(model);
            }

            return RedirectToAction("", "", new { area = "" });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginBindModel login)
        {
            var signInStatus = await SignInManager.PasswordSignInAsync(login.Email, login.Password, true, false);
            if (signInStatus == SignInStatus.Failure)
            {
                var invalidCredentials = "Invalid Credentials";
                ModelState.AddModelError("", invalidCredentials);
                return RedirectToAction("", "", new { area = ""});
            }

            var identityUser = await UserManager.FindByNameAsync(login.Email);
            ICollection<IdentityUserRole> IdUserRole = identityUser.Roles;
            ICollection<IdentityRole> UserRoles = AppRoleManager.Roles.ToList();
            string role = UserRoles.Join(IdUserRole, ur => ur.Id, iur => iur.RoleId, (r, iur) => r.Name).First();

            if (role == "Admin")
                return RedirectToAction("Index", "Home", new { area="Admin"});
            else if (role == "Client")
                return RedirectToAction("Index", "Home");
            else
                return RedirectToAction("", "", new { area = "" });
        }

        [HttpGet]
        public ActionResult Logout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();
            return RedirectToAction("", "", new { area = "" });
        }

        [Authorize(Roles = "Client")]
        [HttpGet]
        public ActionResult Movies(int? pageNo, [ModelBinder(typeof(MovieCustomBinder))]MovieFilters movieFilters)
        {
            var totalMovies = _movieService.CountMovieRecords(movieFilters);
            var paginationInformation = new PaginationInfo(NUMBER_OF_MOVIES_PER_PAGE, pageNo, totalMovies);
            var movies = _movieService.FetchMovies(movieFilters, paginationInformation);
            var viewModel = _mapper.Map<List<MovieDataViewModel>>(movies);
            AdminControllerUtils.SetPaginationViewData(ViewData, paginationInformation);
            AdminControllerUtils.SetPaginationURLSearchTitle(ViewData, movieFilters);
            return View(viewModel);
        }

        [Authorize(Roles = "Client")]
        [HttpGet]
        public ActionResult MovieCards(int? pageNo, [ModelBinder(typeof(MovieCustomBinder))]MovieFilters movieFilters)
        {
            var totalMovies = _movieService.CountMovieRecords(movieFilters);
            var paginationInformation = new PaginationInfo(NUMBER_OF_MOVIES_PER_PAGE, pageNo, totalMovies);
            var movies = _movieService.FetchMovies(movieFilters, paginationInformation);
            var viewModel = _mapper.Map<List<MovieDataViewModel>>(movies);
            AdminControllerUtils.SetPaginationViewData(ViewData, paginationInformation);
            AdminControllerUtils.SetPaginationURLSearchTitle(ViewData, movieFilters);
            return View(viewModel);
        }
    }
}