﻿using System.Web.Mvc;

namespace VideoClub.Web.Areas.user
{
    public class userAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "user";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "user_area_router",
                "user/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "VideoClub.Web.Areas.user.Controllers" }
            );
        }
    }
}