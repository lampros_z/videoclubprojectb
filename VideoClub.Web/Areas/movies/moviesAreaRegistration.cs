﻿using System.Web.Mvc;

namespace VideoClub.Web.Areas.movies
{
    public class moviesAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "movies";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "movies_default",
                "movies/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "VideoClub.Web.Areas.movies.Controllers" }
            );
        }
    }
}