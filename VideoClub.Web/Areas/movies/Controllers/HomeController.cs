﻿using AutoMapper;
using System.Collections.Generic;
using System.Web.Mvc;
using VideoClub.Core.Interfaces;
using VideoClub.Core.Utilities.Areas.Controllers;
using VideoClub.Web.CustomBinders;
using VideoClub.Web.Models.View;

namespace VideoClub.Web.Areas.movies.Controllers
{
    [Authorize(Roles = "Admin")]
    public class HomeController : Controller
    {

        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public HomeController(IUserService userService, IMovieService movieService, IMapper mapper)
        {
            _userService = userService;
            _movieService = movieService;
            _mapper = mapper;
        }

        [HttpPost]
        public ActionResult RentMovie([ModelBinder(typeof(RentalRecordInfoCustomBinder))]RentalRecordInfo info, MovieRentalRecordBindModel[] model)
        {
            var movieCopies = _movieService.FetchMovieCopies(model);
            var rentalRecord = _userService.RegisterUsersMovieRentalRecord(info);
            _movieService.BindMovieRentalRecordWithMovieCopies(rentalRecord, movieCopies);
            return Json(movieCopies, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchMovie(string title)
        {
            var movies = _movieService.GetMoviesWithAvailableMovieCopiesByTitle(title);
            var MovieDataListViewModel = _mapper.Map<List<MovieListViewModel>>(movies);
            return Json(MovieDataListViewModel, JsonRequestBehavior.AllowGet);
        }


        // GET: movies/Home
        public ActionResult Index()
        {
            return View();
        }

    }
}