﻿var filters = [];
var cache = new Set();

$(document).ready(function () {
    "use strict";
    var pluginName = "selectionator";
    var defaults = {
        propertyName: "selectionator",
        src: null,
        orgElement: null,
        checkedItems: [],
        onError: function (error) { }
    };
    function Plugin(element, options) {
        this.element = element;
        this.selector = null;
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }
    Plugin.prototype = {
        init: function () {
            var that = this;
            var self = $(that.element);
            that.options.src = that.element.getAttribute('data-src');
            that.selector = that.createFromJson(that.options.data);
            that.options.orgElement = that.element.parentNode.replaceChild(that.selector, that.element);
            $(that.selector).addClass(that._name);
        },
        createFromJson: function (options) {
            var that = this;
            var select = document.createElement('select');
            var popup = document.createElement('div');
            var header = document.createElement('div');
            var search = document.createElement('span');
            var overlay = document.createElement('span');
            overlay.className = 'overlay';
            var shadow = document.createElement('span');
            shadow.className = 'shadow';
            var placeholder = document.createTextNode('Options');
            search.className = 'search';
            search.appendChild(shadow);
            search.appendChild(overlay);
            search.appendChild(placeholder);
            popup.appendChild(search);
            var menu = document.createElement('ul');
            select.style.display = 'none';
            menu.className = 'list';
            var box = document.createElement('div');
            box.className = 'menu';
            box.appendChild(menu);
            popup.appendChild(box);
            options.optgroups.forEach(function (optgroup, index) {


                var menuItem = document.createElement('li');
                var header = document.createElement('span');
                header.className = 'header';
                var caption = document.createTextNode(optgroup.label);
                header.appendChild(caption);
                menuItem.appendChild(header);
                var menuItems = document.createElement('ul');
                menuItems.className = 'optgroup';
                menuItem.appendChild(menuItems);
                menu.appendChild(menuItem);

                optgroup.options.forEach(function (option, index) {
                    var opt = new Option(option.text, option.value, option.defaultSelected, option.selected);
                    select.options.add(opt);
                    var item = document.createElement('li');
                    var label = document.createElement('label');
                    label.setAttribute("for", option.value);
                    var checkbox = document.createElement('input');
                    $(checkbox).data(option);
                    checkbox.setAttribute('type', 'checkbox');
        
                    checkbox.addEventListener('change', function (event) {
                        var checkbox = event.target;
                        var $el = $(event.srcElement);
                        if (checkbox.checked) {
                            if (isNaN(parseInt(event.srcElement.id))) {
                                if (event.srcElement.id === "false") {
                                    cache.add(false);
                                } else if (event.srcElement.id === "true") {
                                    cache.add(true);
                                }
                            } else if (parseInt(event.srcElement.id)) {
                                cache.add(parseInt(event.srcElement.id))
                            }
                            that.options.checkedItems.push(event.srcElement);
                            if (JSON.parse(sessionStorage.getItem("cache")) != null)
                                placeholder.nodeValue = "Selected: " + (that.options.checkedItems.length + JSON.parse(sessionStorage.getItem("cache")).length) + " out of " + $(that.selector).find('input[type="checkbox"]').length;
                            else
                                placeholder.nodeValue = "Selected: " + that.options.checkedItems.length + " out of " + $(that.selector).find('input[type="checkbox"]').length;
                        } else {
                            let elem = that.options.checkedItems.pop();
                            if (elem == undefined)
                                elem = event.srcElement.id;
                            if (isNaN(elem)) {
                                if (elem === "false") {
                                    cache.delete(false);
                                } else if (elem === "true") { 
                                    cache.delete(true);
                                }
                            }else if (parseInt(elem)) {
                                    cache.delete(parseInt(elem))
                            }
                            if (sessionStorage.getItem("cache") != null) {
                                let cacheArray = JSON.parse(sessionStorage.getItem("cache"))
                                let cacheSet = new Set(cacheArray);
                                if (isNaN( parseInt(elem) ))
                                    cacheSet.delete((elem==="true"));
                                else
                                    cacheSet.delete(parseInt(elem));
                                cacheArray = Array.from(cacheSet);
                                sessionStorage.setItem("cache", JSON.stringify(cacheArray));
                                placeholder.nodeValue = "Selected: " + (that.options.checkedItems.length + cacheArray.length) + " out of " + $(that.selector).find('input[type="checkbox"]').length;
                            } else {
                                placeholder.nodeValue = "Selected: " + that.options.checkedItems.length + " out of " + $(that.selector).find('input[type="checkbox"]').length;
                            }
                            
                            that.options.checkedItems = that.options.checkedItems.filter(function (items, index) {
                                return items.value != $el.data().value;
                            });
                        }
                        filters = [];
                        for (let item of that.options.checkedItems) {
                            filters.push(item.id);
                        }
                    });
                    checkbox.id = option.value;
                    var caption = document.createTextNode(option.text);
                    label.appendChild(caption);
                    item.appendChild(checkbox);
                    item.appendChild(label);
                    menuItems.appendChild(item);
                });
            });
            return popup;
        },
        onAdd: function (data) {
            var that = this;
            return that.options.onAdd(that, data);
        },
        onRemove: function (data) {
            var that = this;
            var self = $(that.element);
            return that.options.onRemove(data);
        },
        destroy: function () {
            var that = this;
            $(that.element).unbind("destroyed", that.teardown);
            that.teardown();
        },
        teardown: function () {
            var that = this;
            $(that.element).removeClass(that._name);
            $(that.selector).replaceWith(that.options.orgElement);
            $(that.element).removeData(that._name);
            that.unbind();
            that.element = null;
        },
        bind: function () { },
        unbind: function () { }
    };
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, pluginName)) {
                $.data(this, pluginName, new Plugin(this, options));
            }
        });
    };
});

$(document).ready(function () {
    $('#select').selectionator({
        data: {
            optgroups: [{
                label: 'Genre',
                options: [{
                    value: 1,
                    text: 'Action',
                    defaultSelected: true,
                    selected: false
                }, {
                    value: 2,
                    text: 'Adventure',
                    defaultSelected: false,
                    selected: false
                }, {
                    value: 3,
                    text: "Crime",
                    defaultSelected: false,
                    selected: false
                    },
                    {
                    value: 4,
                    text: "Mystery",
                    defaultSelected: false,
                    selected: true
                    },
                    {
                        value: 5,
                        text: "Comedy",
                        defaultSelected: false,
                        selected: true
                    },
                    {
                        value: 6,
                        text: "Drama",
                        defaultSelected: false,
                        selected: true
                    },
                    {
                        value: 7,
                        text: "Fantasy",
                        defaultSelected: false,
                        selected: true
                    },
                    {
                        value: 8,
                        text: "Historical",
                        defaultSelected: false,
                        selected: true
                    },
                    {
                        value: 9,
                        text: "Horror",
                        defaultSelected: false,
                        selected: true
                    },
                    {
                        value: 10,
                        text: "Philosophical",
                        defaultSelected: false,
                        selected: true
                    },
                    {
                        value: 11,
                        text: "Political",
                        defaultSelected: false,
                        selected: true
                    },
                    {
                        value: 12,
                        text: "Romance",
                        defaultSelected: false,
                        selected: true
                    },
                ]
            }, {
                label: 'Copies',
                options: [{
                    value: true,
                    text: 'Available',
                    defaultSelected: false,
                    selected: false
                }, {
                    value: false,
                    text: 'Unavailable',
                    defaultSelected: false,
                    selected: false
                }]
            }]
        }
    });
    setTimeout(function () {
        $(".selectionator").addClass('opened');
    }, 500);
    setTimeout(function () {
        $(".selectionator").removeClass('opened');
    }, 1250);
});
