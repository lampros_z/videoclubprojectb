﻿namespace VideoClub.Web.LocalStrings
{
    public class CustomBinderStrings
    {
        public static string AVAILABILITY_FILTER_URL_QUERY_PARAM_KEY = "availability";
        public static string GENRE_FILTER_URL_QUERY_PARAM_KEY = "genres";
        public static string TITLE_FILTER_URL_QUERY_PARAM_KEY = "Title";
        public static string USER_ID_URL_QUERY_PARAM_KEY = "UserId";
        public static string COMMENT_TEXT_URL_QUERY_PARAM_KEY = "CommentText";

        private CustomBinderStrings() { }
    }
}