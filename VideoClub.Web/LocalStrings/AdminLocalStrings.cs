﻿namespace VideoClub.Web.LocalStrings
{
    public class AdminLocalStrings
    {
        public static int NUMBER_OF_MOVIES_PER_PAGE = 10;
        public static int NUMBER_OF_RENTAL_RECORDS_PER_PAGE = 10;
        public static int NUMBER_OF_CLIENTS_PER_PAGE = 10;
        public static int NUMBER_OF_RENTAL_HISTORY_RECORDS_PER_PAGE = 10;
        public static int NUMBER_OF_ACTIVE_RENTAL_HISTORY_RECORDS_PER_PAGE = 10;
        public static string PAGINATION_LENGTH_KEY = "pagination";
        public static string PAGINATION_CURRENT_PAGE_KEY = "page";
        public static string PAGINATION_URL_SEARCH_TITLE = "search";
        private AdminLocalStrings() { }
    }
}
