﻿using System.Web.Mvc;
using VideoClub.Core.Utilities.Areas.Controllers;
using static VideoClub.Web.LocalStrings.CustomBinderStrings;

namespace VideoClub.Web.CustomBinders
{
    public class MovieCustomBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var request = controllerContext.HttpContext.Request;
            var AvailabilityFilters = request.QueryString.GetValues(AVAILABILITY_FILTER_URL_QUERY_PARAM_KEY);
            var GenreFilters = request.QueryString.GetValues(GENRE_FILTER_URL_QUERY_PARAM_KEY);
            var TitleSearchText = request.QueryString.Get(TITLE_FILTER_URL_QUERY_PARAM_KEY);
            
            return new MovieFilters(TitleSearchText, GenreFilters, AvailabilityFilters);
        }
    }
}