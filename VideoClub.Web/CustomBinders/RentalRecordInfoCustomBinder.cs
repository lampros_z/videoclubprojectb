﻿using System.Web.Mvc;
using VideoClub.Core.Utilities.Areas.Controllers;
using static VideoClub.Web.LocalStrings.CustomBinderStrings;

namespace VideoClub.Web.CustomBinders
{
    public class RentalRecordInfoCustomBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var request = controllerContext.HttpContext.Request;
            var UserId = request.QueryString.Get(USER_ID_URL_QUERY_PARAM_KEY);
            var CommentText = request.QueryString.Get(COMMENT_TEXT_URL_QUERY_PARAM_KEY);
            return new RentalRecordInfo(UserId, CommentText);

        }
    }
}